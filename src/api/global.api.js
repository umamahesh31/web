import * as api from "./api";

const post = (url, data) => {
  return api
    .postReq(url, data)
    .then((data) => {
      return data;
    })
    .catch((err) => {
      return err;
    });
};

export { post };
