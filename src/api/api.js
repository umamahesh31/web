import axios from "axios";

const postReq = (url, data = {}, headers = {}) => {
  return axios({
    headers: headers,
    method: "post",
    data: data,
    url: getFullURL(url),
  }).catch((error) => {
    return error;
  });
};
const getFullURL = (path) => {
  return process.env.REACT_APP_API_URL + path;
};

export { postReq };
