import React, { useState } from "react";
import { Card } from "primereact/card";
import { InputText } from "primereact/inputtext";
import { Password } from "primereact/password";
import { Button } from "primereact/button";
import * as api from "../api/global.api";

const Login = () => {
  const [user, setuser] = useState({
    logoinName: "",
    password: "",
  });
  const onChange = (e, key) => {
    setuser({
      ...user,
      [key]: e.target.value,
    });
  };
  const addDetailsToDB = () => {
    api
      .post("pratice/add", user)
      .then((data) => {
        console.log(data);
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className="outer">
      <Card className="cardDec" title="Sample fields">
        <InputText
          className="m6 w100"
          value={user.username}
          onChange={(e) => onChange(e, "logoinName")}
          placeholder="Username or Email address"
        />
        <Password
          className="m6 "
          value={user.password}
          onChange={(e) => onChange(e, "password")}
          placeholder="Password"
          feedback={false}
          toggleMask
        />
        <Button
          className="m6 w100"
          label="Add Details to DB"
          onClick={addDetailsToDB}
        />
      </Card>
    </div>
  );
};

export default Login;
