FROM node:12.7-alpine AS build
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=0 /src/dist/ ./
RUN ls -la ./*
ENTRYPOINT ["nginx", "-g", "daemon off;"]
